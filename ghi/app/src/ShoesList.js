import React from "react";

export class ShoesList extends React.Component {
    state = {
        shoes: [],
        bins: [],
    }

    async getShoesList() {
        const response = await fetch("http://localhost:8080/api/shoes")

        if (response.ok){
            const data = await response.json();
            const shoes = data.shoes;
            this.setState({shoes:shoes})
            console.log(data)
        }
    }

    async componentDidMount(){
        this.getShoesList()
    }
    async handleDelete(event) {
        const url = `http://localhost:8080/api/shoes/${event}`
        await fetch(url, {method: "DELETE"})
        this.getShoesList()
    }


render() {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Color</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {this.state.shoes.map(shoe =>{
                    return (
                    <tr key = {shoe.id}>
                        <td>{shoe.model_name}</td>
                        <td>{shoe.manufacturer}</td>
                        <td>{shoe.color}</td>
                        <td>{shoe.bin.closet_name}, #{shoe.bin.bin_number}</td>
                        <td><img src = {shoe.picture_url} width="100" height="100"/></td>
                        <td><button onClick={() =>this.handleDelete(shoe.id)}
                            >Delete</button></td>
                </tr>
                    )
                })}

            </tbody>
        </table>
    )
}
}
