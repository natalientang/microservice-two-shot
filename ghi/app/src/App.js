import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatForm from './HatForm';
import HatList from './HatList';
import MainPage from './MainPage';
import Nav from './Nav';
import { ShoesList } from './ShoesList';
import { ShoesForm } from './ShoesForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path = "hats">
            <Route path="" element={<HatList />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="shoes" element={<ShoesList/>} />
          <Route path="shoes/new" element={<ShoesForm/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
