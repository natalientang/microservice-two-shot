import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fabric: "",
            style_name: "",
            color: "",
            url: "",
            locations: [],
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleUrlChange = this.handleUrlChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
    }

    async handleCreate(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        console.log(data);

        const hatsUrl = `http://localhost:8090/api/hats/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(hatsUrl, fetchConfig);
        if(response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            const cleared = {
                fabric: "",
                style_name: "",
                color: "",
                url: "",
                location: "",
            };
            this.setState({ success: true });
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations/"

        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            console.log(data);
            this.setState({ locations: data.locations });
        }
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
    }
    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({ style_name: value });
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }
    handleUrlChange(event) {
        const value = event.target.value;
        this.setState({ url: value });
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value });
    }


    render() {
        let notSubmittedClass = "not-submitted";
        let successClass = "alert alert-success d-none mb-0";

        if (this.state.success === true) {
          notSubmittedClass = "not-submitted d-none";
          successClass = "alert alert-success mb-0";
        }

        let spinnerClasses = "d-flex justify-content-center mb-3";
        let dropdownClasses = "form-select d-none";
        if (this.state.locations.length > 0) {
          spinnerClasses = "d-flex justify-content-center mb-3 d-none";
          dropdownClasses = "form-select";
        }
    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form onSubmit={this.handleCreate} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleStyleNameChange} value={this.state.style_name} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                  <label htmlFor="style_name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleUrlChange} value={this.state.url} placeholder="URL" required type="text" name="url" id="url" className="form-control"/>
                  <label htmlFor="url">URL</label>
                </div>
                <div className="mb-3">
                    <select onChange={this.handleLocationChange} name="location" id="location" className={dropdownClasses} required>
                      <option value="">Choose a location</option>
                      {this.state.locations.map((location) => {
                        return (
                          <option key={location.id} value={location.href}>
                            {location.closet_name}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                <div>
                <button className="btn btn-primary mb-3">Create a hat</button>
                </div>
                <div className={successClass} id="success-message">
                    You have created a new hat!
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
    }
}

export default HatForm;
