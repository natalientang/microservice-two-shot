import React from "react";

export class ShoesForm extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            model_name: '',
            manufacturer: '',
            color: '',
            picture_url: '',
            bin:'',
            bins: [],

        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }


    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json()
            this.setState({bins: data.bins})
            console.log(data)
        }
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log(data)
        delete data.bins

        const url = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            this.setState({
                model_name: '',
                manufacturer: '',
                color: '',
                picture_url: '',
                bin:'',
            });
            this.setState({success: true})
        }

    }
    handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]: value});
    }


    render(){

        let notSubmittedClass = "not-submitted";
        let successClass = "alert alert-success d-none mb-0";

        if (this.state.success === true) {
          notSubmittedClass = "not-submitted d-none";
          successClass = "alert alert-success mb-0";
        }

        let spinnerClasses = "d-flex justify-content-center mb-3";
        let dropdownClasses = "form-select d-none";
        if (this.state.bins.length > 0) {
          spinnerClasses = "d-flex justify-content-center mb-3 d-none";
          dropdownClasses = "form-select";
        }

        return(
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add some Shoes</h1>
                    <form onSubmit={this.handleSubmit} id="create-shoes-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                        <label htmlFor="model_name">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChange} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                        <label htmlFor="picture_url">Picture Url</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleChange} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a Bin</option>
                        {this.state.bins.map((bin) => {
                            return (
                            <option key={bin.id} value={bin.href}>{bin.closet_name}</option>

                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary mb-3">Create</button>
                    </form>
                    <div className={successClass} id = "success-message">You have created a new shoes!</div>
                </div>
                </div>
            </div>
        )
    }
}
