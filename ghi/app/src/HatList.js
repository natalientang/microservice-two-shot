import React from 'react';

class HatList extends React.Component {
    state = {
        hats: [],
        locations: []
    }

    async getHatList() {
        const response = await fetch("http://localhost:8090/api/hats")
        if(response.ok) {
            const data = await response.json();
            const hats = data.hats;
            this.setState({hats: hats});
            console.log(data)
        }
    }

    async componentDidMount() {
        this.getHatList();
    }

    async handleDelete(event) {
        const url = `http://localhost:8090/api/hats/${event}`
        await fetch(url, {method: "DELETE"})
        this.getHatList()
    }

    render() {
        return (
        <table className="table table-striped">
            <thead>
              <tr>
                <th>Fabric</th>
                <th>Style Name</th>
                <th>Color</th>
                <th>Location/Shelf Number</th>
                <th>Image</th>
              </tr>
            </thead>
            <tbody>
              {this.state.hats.map(hat => {
                return (
                  <tr key={hat.id}>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.color }</td>
                    <td>{ hat.location.closet_name }, #{ hat.location.shelf_number }</td>
                    <td><img src={ hat.url } width="100" height="100"/></td>
                    <td><button onClick={() => this.handleDelete(hat.id)}>Delete</button></td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        );
    }
}

export default HatList;
