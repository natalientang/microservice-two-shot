import json
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Hat, LocationVO


# Singles out the specific field from the LocationVO to display on page
class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href"
    ]

# Shows the list of hats on the page
class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "url",
        "id",
        "location"
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }

# Shows all of the details of the hat on the page
class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }

# Gets a Hat's Details
@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    # Lists the names of hats and the link to the hat for the specified location id
    # Returns a dictionary with a single key "hats" which is a list of hat names and URLs. Each entry in the list contains name of the hat and the link to the hat's info
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        # Creates a hat
        content = json.loads(request.body)
        # Gets the Location object and puts it in the content dictionary
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


# Deletes a Hat
@require_http_methods(["DELETE", "GET"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
