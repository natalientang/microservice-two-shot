from django.db import models


# Refers back to the Location model in wardrobe
class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.closet_name

# Hat model represents someone that wants to input a hat
class Hat(models.Model):
    fabric = models.CharField(max_length=20)
    style_name = models.CharField(max_length=20)
    color = models.CharField(max_length=20)
    url = models.URLField(max_length=100)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE

    )

    def __str__(self):
        return self.fabric
