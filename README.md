# Wardrobify

Team:

* Person 1 - Alonso Rodriguez (Shoes)
* Person 2 - Natalie Tang (Hats)

## Design

We use Location and Bin VOs to access the data with polling from the Wardrobe api in our own microservices.

## Shoes microservice

I created two models one for the VO to use the data polled from wardrobes api. The other was a shoe model used for the views to create shoes using the fields given and adding a foreignkey to connect it to the BinVO. Then I used encoders to help establish what I needed in data for the views to use in React when i got there. I had to put bin as a property of my encoders to get the specific bin tied to shoe to show in my list.

## Hats microservice

A "Hat" model needs to be created to represent someone that wants to input a hat which includes all of the properties and the identified foreign key. The foreign key in this instance would be the "location" so a location value object also needs to be created in the models. This would then allow the hats microservice to ask or poll a list of properties that we need from the wardobe microservice to retrieve data and store it in our specific hats microservice to use locally.
