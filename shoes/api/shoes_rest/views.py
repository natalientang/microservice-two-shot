from django.shortcuts import render
from .models import  Shoes, BinVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder

class BinVOListEncoder(ModelEncoder):
    model = BinVO
    properties = [ "import_href","closet_name","bin_number","bin_size", "id"]

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["model_name","manufacturer", "color","picture_url","id", "bin"]

    encoders = {"bin": BinVOListEncoder()}



class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ["manufacturer", "model_name", "color", "picture_url",
    "bin"]

    encoders={
        "bin": BinVOListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if  request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder= ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            print(content)
            bin = BinVO.objects.get(import_href=content["bin"])

            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin"},
                status=400,
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False
        )
@require_http_methods(["GET", "DELETE"])
def api_show_shoes(request,id):
    if request.method  == "GET":
        shoe = Shoes.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False
        )
    else:
        try:
            shoe = Shoes.objects.get(id=id)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoesDetailEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
